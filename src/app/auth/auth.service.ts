import { Subject } from 'rxjs';

import { User } from './user.model';
import { AuthData } from './auth-data.model';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { TrainingService } from '../training/training.service';

@Injectable()
export class AuthService {
  //Its like an event emitter, but its with rxjs, its uses next instead of emit
  authChange = new Subject<boolean>();
  private authenticated = false;
  
  constructor( private router: Router, private fireAuth: AngularFireAuth, private trainingService: TrainingService) {

  }

  initAuthListener(){
    this.fireAuth.authState.subscribe(user => {
      if (user) {
        this.authenticated = true;
        this.authChange.next(true);
        this.router.navigate(['/training']);
      } else {

      }
    })
  }

  registerUser(authData: AuthData) {
    this.fireAuth.createUserWithEmailAndPassword(
      authData.email,
      authData.password
    ).then(result => {
      console.log(result);
      this.authSuccess();
    }).catch(error => {
      console.log(error);
    });
    this.authSuccess();
  }

  login(authData: AuthData) {
    this.fireAuth.signInWithEmailAndPassword(authData.email, authData.password).then(result => {
      console.log(result);
      this.authSuccess();
    }).catch(error => {
      console.log(error);
    });
    this.authSuccess();
  }

  logout() {
    this.trainingService.cancelSubscriptions();
    this.authenticated = false;
    this.authChange.next(false);
    this.router.navigate(['/login']);
  }

  isAuth() {
    return this.authenticated;
  }

  private authSuccess(){
    this.authenticated = true;
    this.authChange.next(true);
    this.router.navigate(['/training']);
  }
}
