import { Exercise } from "./exercise.model";
import { Subject } from "rxjs";
import { AngularFirestore } from "@angular/fire/compat/firestore";
import { map } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { Subscription } from "rxjs";

@Injectable()
export class TrainingService {
    exerciseChanged = new Subject<Exercise>();
    exercisesChanged = new Subject<Exercise[]>();
    listExercisesChanged = new Subject<Exercise[]>();
    private availableExercises: Exercise[] = [];
    private runningExercise!: Exercise;
    private exercises: Exercise[] = [];
    constructor(private db: AngularFirestore) {}
    private fbSubs: Subscription[] = [];

  fetchAvailableExercises() {
    this.fbSubs.push(this.db
      .collection('availableExercises')
      .snapshotChanges()
      .pipe(map((docArray: any[]) => {
        return docArray.map((doc) => {
          return {
            id: doc.payload.doc.id,
            name: doc.payload.doc.data()['name'],
            duration: doc.payload.doc.data()['duration'],
            calories: doc.payload.doc.data()['calories']
          };
        });
      }))
      .subscribe((exercises: Exercise[]) => {
        this.availableExercises = exercises;
        this.exercisesChanged.next([...this.availableExercises]);
      }, error => {
        console.log(error);
      }));
  }

  getAvailableExercises(){
        return this.availableExercises.slice();
    }

    startExercise(selectedId: string){
        this.runningExercise = this.availableExercises.find(
            ex => ex.id === selectedId)!;
        this.exerciseChanged.next({...this.runningExercise});
    }
    
    completeExercise() {
        this.addDataToDB({
          ...this.runningExercise,
          date: new Date(),
          state: 'completed'
        });
        this.runningExercise = null!;
        this.exerciseChanged.next(null!);
    }
    
    cancelExercise(progress: number) {
        this.addDataToDB({
          ...this.runningExercise,
          duration: this.runningExercise.duration * (progress / 100),
          calories: this.runningExercise.duration * (progress / 100),
          date: new Date(),
          state: 'cancelled'
        });
        this.runningExercise = null!;
        this.exerciseChanged.next(null!);
    }

    getRunningExercise() {
        return { ...this.runningExercise};
    }

      fetchExerciseList() {
        this.fbSubs.push(this.db
        .collection('finishedExercises')
        .valueChanges()
        .subscribe((exercises: any) => {
            this.listExercisesChanged.next(exercises)
          }, error => {
            console.log(error);
      }));
      }

      cancelSubscriptions(){
        this.fbSubs.forEach(sub => sub.unsubscribe());
      }

      private addDataToDB(exercise: Exercise){
        this.db.collection('finishedExercises').add(exercise);
      }
}